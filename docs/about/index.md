# About me

<div style="display: flex;justify-content: center;"><img src="../images/DISCOPORTRAIT.png"></div>  

Heij!  
I am a Computer Science for Media and Communication / Medien- und Kommunikationsinformatik student at HSRW.  
In this wiki I will document the content of the course 'Fundamentals of Digital Fabrication' and the projects I'll do alongside it.  
  
I always liked to make stuff and fiddling around with various materials I could get my hands on. Gardening is one of my greatest delights! So I think most of my projects will circle back to plants and garden stuff :D!  
Another hobby of mine is making illustration and in general artsy stuff, so I hope I can incorporate some of it in the process.  

<div style="display: flex;justify-content: center;">- ✿ ✿ ✿ - </div>
 
