#define SOIL_PIN A0

//LED Pin Array
int leds[] = {4,5,6,7};

// constant values to map the data of the soil moisture sensor to
// they have to be measured again, if the setup is rebuild!
const int dry = 817;
const int wet = 540;

int moistureReading;
int moistureMapped;

void setup()
{
  Serial.begin(9600);
  // Setting all LED pins to Output
  pinMode(leds[0], OUTPUT);
  pinMode(leds[1], OUTPUT);
  pinMode(leds[2], OUTPUT);
  pinMode(leds[3], OUTPUT);
  // the only input device this time
  // the soil moisture sensor 
  pinMode(SOIL_PIN, INPUT);
}

void loop()
{
  // reading the output of the soil moisture sensor 
  moistureReading = analogRead(SOIL_PIN);

  // mapping the reading to 0 - 4 
  // represents the amount of LEDs lit up
  moistureMapped = constrain(map(moistureReading, dry, wet, 0, 4), 0, 4);

  // only debugging purposes  
  Serial.print("reading: ");
  Serial.println(moistureReading);
  Serial.print("LEDs on: ");
  Serial.println(moistureMapped);

  // iterating over the LEDs to switch them on or of
  for(int i = 0; i < 4; i++) {

    // if the current LED-count is in the range of LEDs
    // previously mapped from the moisture reading
    // it gets switched on, otherwise it's switched of
    if(i+1 <= moistureMapped) {
      digitalWrite(leds[i], HIGH);      
    } else {
      digitalWrite(leds[i], LOW);
    }
  }

 // updates every second
 delay(1000);   
}
