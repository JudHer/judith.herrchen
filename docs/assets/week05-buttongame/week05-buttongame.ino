const byte ledPin = 13; // hardwired LED on pin 13
const byte btnPin = 7;  // button on pin 7
boolean btnstatus;

int quest() {
  int blinks = random(1, 6);  // a random number between 1 and 5 gets created
  for(int i = 1; i <= blinks; i++) {  // the LED blinks for the earlier chosen random number
    digitalWrite(ledPin, HIGH);
    delay(150);
    digitalWrite(ledPin, LOW);
    delay(500);
  }
  return blinks; // the random number gets returned
}

boolean turn(int quest) {
  int turncounter = 0;
  int turnTime = 1500 * quest; // turnTime gets set depending how many blinks the LED made
  unsigned long turnStart = millis();
  unsigned long turnCurrTime = turnStart;
  
  while((turnCurrTime - turnStart) < turnTime ) { // the time the task can be executed is limited
    turnCurrTime = millis(); // the current turn time is saved to be able to compare how much time has passed since the start
    btnstatus = digitalRead(btnPin); 

    while(!btnstatus){ // while the button is pushed

      digitalWrite(ledPin, HIGH); // LED gets switched on
      btnstatus = digitalRead(btnPin);  // the button status is read again to notice a change

      if(btnstatus){ // button isn't pushed, so is released again
        turncounter++; // button presses gets counted +1
        digitalWrite(ledPin, LOW); // the LED gets switched off again
        delay(50);
      }
    }
  }

  return turncounter == quest; // returns whether the player won or not
}

void setup() {
  Serial.begin(9600); //set baud rate for serial data transmission
  pinMode(btnPin, INPUT_PULLUP); 
  pinMode(ledPin, OUTPUT);
}

void loop() {  
  // just feedback on the Serial Monitor
  Serial.println("Watch the LED!");
  delay(1500);
  Serial.println("ready!");
  delay(250);
  Serial.println("steady!");
  delay(250);
  Serial.println("go!");  

  int blinks = quest(); // quest() gets called, to flash the LED random times and get this number for later use
  
  delay(200);
  Serial.println("now it's your turn!");
  
  if(turn(blinks)) {  // turn() gets called and returns wether the player suceeded or not
    Serial.println("you won!");
  } else {
    Serial.println("oww, you lost.. but try again!");
  }

  delay(5000); // after 5 seconds the game restarts
  
}

// wasn't much planty and flowery stuff going on this week so:
// some  ✿ ✿ ✿ 