#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_LEDBackpack.h>

Adafruit_8x8matrix matrix = Adafruit_8x8matrix();

void setup() {
  Wire.begin();
  
  Serial.begin(9600);
  Serial.println("8x8 LED Matrix Test");
  
  matrix.begin(0x70);  // pass in the address
  // I tested to set the brightness, it worked 
  // ranges from 0 to 15
  matrix.setBrightness(0);
  // tried setting the cursor somewhere else
  //matrix.setCursor(0,0);
  // tried rotating, it worked, but only the same LED lit up 
  // just the order in which they blinked were different, when iterating over them
  //matrix.setRotation(3)
}

// tried two different variants of representing bmp images
/*static const uint8_t PROGMEM
  full_bmp[] =
  { B11111111,
    B11111111,
    B11111111,
    B11111111,
    B11111111,
    B11111111,
    B11111111,
    B11111111 };
    */

static const uint8_t PROGMEM full_bmp[]={0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

void loop() {

// small for loop to flash each LED one after another
/*  
 for(uint8_t x = 0; x < 8; x++) {  
    for(uint8_t y = 0; y < 8; y++) {
      // just for debugging the debugging
      Serial.print(x);
      Serial.print(", ");
      Serial.println(y);
      
      matrix.clear();
      matrix.drawPixel(x, y, LED_ON);
      matrix.writeDisplay();
      delay(100);
    }
  }
  */

  // drawing the bitmap
  matrix.clear();
  matrix.drawBitmap(0, 0, full_bmp, 8, 8, LED_ON);
  matrix.writeDisplay();
  delay(500);
  
}
