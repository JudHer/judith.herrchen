#include <DHT.h>
#include <DHT_U.h>

// defining all the pin
#define DHTTYPE DHT11
#define DHT_PIN A2
#define SOIL_PIN A0
#define LDR_PIN A4

// constant values to map the data of the soil moisture sensor to
// they have to be measured again, if the setup is rebuild!
const int dry = 817;
const int wet = 480;
// a counter to repeat the header of the printed table every once in a while
byte turncounter = 0; 

DHT dht(DHT_PIN, DHTTYPE);

// function for printing the table header, nothing exiting 
void printHeader() {
  Serial.print("| Light Level |");
  Serial.print(" Air Humidity |");
  Serial.print(" Temperature |");
  Serial.println(" Soil Moisture |");
}

void setup() {
  // defining the pinmodes, all Input
  pinMode(SOIL_PIN, INPUT);
  pinMode(DHT_PIN, INPUT);
  pinMode(LDR_PIN, INPUT);
  Serial.begin(9600);
  dht.begin();

  // printing the logo for the small Plant Weather Station :)
  Serial.println("         ___");
  Serial.println("-.    __/   \\__");
  Serial.println("\\ `-./  \\,-./  \\ ");
  Serial.println(" \\ -.>--(   )--<  THE PLANT WEATHER STATION ");
  Serial.println("  `-.\\__/`-´\\__/ ");
  Serial.println("        \\___/ \n");
  printHeader();
}

void loop() {
  // reading the data of the DHT11
  int humidityReading = dht.readHumidity();
  int temperatureReading = dht.readTemperature();

  // reading the data of the soil moisture sensor
  int moistureReading = analogRead(SOIL_PIN);
  // mapping the raw data to the pre-measured values for dry and wet 
  // and constraining them to not overstep 0 or 100 %
  int moistureMapped = constrain(map(moistureReading, dry, wet, 0, 100), 0, 100);

  // reading the data of the LDR and mapping them 
  int ldrReading = analogRead(LDR_PIN);
  int ldrMapped = map(ldrReading, 0, 1023, 0, 100);

  // printing the measurements
  Serial.print("|     ");
  Serial.print(ldrMapped);
  Serial.print(" %    |      ");
  Serial.print(humidityReading);
  Serial.print(" %    |     ");
  Serial.print(temperatureReading);
  Serial.print(" °C   |      "); 
  Serial.print(moistureMapped);
  Serial.println(" %   |");

  // printing the header every 15 lines
  if (turncounter == 15 ) {
    turncounter = 0;
    printHeader();
  } else {
    turncounter++;
  }

  delay(2000);
}
