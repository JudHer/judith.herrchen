#include <OneWire.h>
#include <DallasTemperature.h>

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_LEDBackpack.h>

Adafruit_8x8matrix matrix = Adafruit_8x8matrix();

// defining all the pins
#define SOIL_PIN A0
#define LDR_PIN A2
#define TEMP_PIN 3

OneWire oneWire(TEMP_PIN); 
DallasTemperature sensors(&oneWire);

// constant values to map the data of the soil moisture sensor to
// they have to be measured again, if the setup is rebuild!
const int dry = 817;
const int wet = 480;
// just two variables to make the configuration easier 
// 0 and 30 degree are picked quite random 
// I hope the temperature of my room will never drop to 0 degrees 
const int maxTemp = 30;
const int minTemp = 0;

void setup() {
  // defining the pinmodes, all Input
  pinMode(SOIL_PIN, INPUT);
  pinMode(LDR_PIN, INPUT);
  sensors.begin();
  matrix.begin(0x70);
  // The LED-Matrix has to be rotated since I screwed it on flipped
  matrix.setRotation(1);
}

void loop() {

  // reading the data of the LDR 
  int ldrReading = analogRead(LDR_PIN);
  
  // if it's bright enough measure the other stuff and display it
  if(ldrReading > 102) {       
    
    // reading the data of the soil moisture sensor
    int moistureReading = analogRead(SOIL_PIN);
    // request reading of the temperature sensor
    sensors.requestTemperatures();
    float temp = sensors.getTempCByIndex(0);
    
    // mapping the ldr reading that was measured before hand   
    // it gets mapped from 0 to 8 so it can be displayed on the 8 LEDs of the LED-Matrix
    int lightMatrixMap = map(ldrReading,0, 1023, 0, 8); 
    
    // mapping the soil moisture reading to the pre-measured values for dry and wet 
    // same as with the LDR mapping, from 0 to 8 because of the LED count
    // additionally the values had to be constrain so to not overstep 0 or 8 
    // since dry and wet doesn't reach from 0 to 1023 and can overstep when measured
    int soilMatrixMap = constrain(map(moistureReading, dry, wet, 0, 8), 0, 8);

    // since apparently there has to be a check for the connection 
    // the variable gets initialised with 0 and later assign the actual temperature value
    // like before it gets mapped to 0 and 8 and gets constrained
    int tempMatrixMap = 0;
    if(temp != DEVICE_DISCONNECTED_C) 
    {
      tempMatrixMap = constrain(map(temp, minTemp, maxTemp, 0, 8), 0, 8);
    }
     
    // The matrix gets cleared before drawing so the old bars disappear
    // so all LEDs get switched off
    matrix.clear();
    // Temperature Bar
    matrix.drawRect(0,0, tempMatrixMap, 2, LED_ON);
    // Soil Moisture Bar
    matrix.drawRect(0,3, soilMatrixMap, 2, LED_ON);
    // Light Level Bar
    matrix.drawRect(0,6, lightMatrixMap, 2, LED_ON);
    // when all changes are made the according LEDs get switched on
    matrix.writeDisplay(); 
  } else {
    // Matrix doesn't displays anything if it's too dark 
    // don't want to get disturbed in my sleep
    matrix.clear();
    matrix.writeDisplay();
  }
  delay(2000);
}
