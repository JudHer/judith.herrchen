# week 07: Output devices

## Summary

After the input device week there's of course the output device week! This week we got to know some output devices and how to use them. In this example I initially tried using a LED matrix, which failed due to problem I couldn't solve. Soo there had to be some improvisations.

## Parts

|Quantity|  |
|-|-|
|1x| Arduino Uno |
|1x| Capacitive Soil Moisture Sensor v1.2 + the connector cables |
|4x| LEDs |
|4x| 1 kOhm resistors |
|1x| Breadboard |
|5x for LEDs (+ 3x for the Sensor)| Jumper Cables |

## The Setup 

Initially I wanted to test out the 1.2'' LED matrix, though I encountered a strange problem, where just four of the LEDs would light up. I used the [LED Backpack library](https://github.com/adafruit/Adafruit_LED_Backpack) (which depends on the [GFX Library](https://github.com/adafruit/Adafruit-GFX-Library)) provided by adafruit together with the backpack the LED matrix is stuck to. I connected the SCL and SDA to the dedicaded pins on the Arduino (labled with SDA and SCL) though I also tried using A4 and A5 like it's said in the [guide by adafruit](https://learn.adafruit.com/adafruit-led-backpack/1-2-8x8-arduino-wiring-and-setup), but with both setups I got the same result.  
I tried downgrading the libraries and tried the [official example](https://github.com/adafruit/Adafruit_LED_Backpack/blob/master/examples/matrix8x8/matrix8x8.ino) by adafruit.  

![weird four LEDs](../images/week07-image01.jpg)  
_Only four lit up LEDs_
    
Since I wanted to use the LED matrix to display the sensor data in barcharts, e.g. the amount of water in the soil, I tried coming up with an improvisation using the materials I had on hand.  
  
If I'll find a solution to the LED matrix problem, I'll update this chapter.  
  
### The Improvisation

I tried realizing at least one bar of the whole barchart, though this is a really inconvenient solution to the problem, but the best I could manage.  
![improvisational setup](../images/week07-image04.JPG)  
_The improvisational setup_  
![LED closeup](../images/week07-image06.JPG)  
_Closeup of the LEDs_  
  
After the first setup I ran a small code snippet, to make sure everything worked like it should.  
```cpp
int leds[] = {4,5,6,7}; // The LED Pins

void setup()
{
  // all LED pins to Output
  pinMode(leds[0], OUTPUT);
  pinMode(leds[1], OUTPUT);
  pinMode(leds[2], OUTPUT);
  pinMode(leds[3], OUTPUT);
}

void loop()
{  
  // just iterating up and down so every LED gets lit
  for(int i = 0; i < 4; i++) {
     digitalWrite(leds[i], HIGH);
    delay(500); 
  }  
  for(int i = 3; i >= 0; i--) {
    digitalWrite(leds[i], LOW);
    delay(500); 
  }
 delay(500);   
}
```

Later I of course added the soil moisture sensor to have some incoming data to react on.  

![Whole setup](../images/week07-image07.JPG)  
_Setup with soil moisture sensor_  
![Whole setup](../images/week07-image08.png)  
_Clean version_  

## The Programming

I remeasured the max values for _wet_ and _dry_, because this time I only wanted to meassure the water in the soil, not pure water. The value for moist soil always flickered between 530 and 535, so I decided to set it to 540 to have some clearance, since the sensor wasn't that percise.  
  
The soil moisture sensor was the only input I had this time. I used the _map()_ and the _constraint()_ similar to last weeks example, though this time I mapped the values to 0 - 4  to distribute the values on the amount of LEDs that would light up to represent the moisture level.  

Lastly the LED Array got iterated over, to switch the LEDs on or off, depending on the previously mapped value of the soilmoisture reading.   
  
(The code is commented accordingly)  
  
```cpp
#define SOIL_PIN A0

//LED Pin Array
int leds[] = {4,5,6,7};

// constant values to map the data of the soil moisture sensor to
// they have to be measured again, if the setup is rebuild!
const int dry = 817;
const int wet = 540;

int moistureReading;
int moistureMapped;

void setup()
{
  Serial.begin(9600);
  // Setting all LED pins to Output
  pinMode(leds[0], OUTPUT);
  pinMode(leds[1], OUTPUT);
  pinMode(leds[2], OUTPUT);
  pinMode(leds[3], OUTPUT);
  // the only input device this time
  // the soil moisture sensor 
  pinMode(SOIL_PIN, INPUT);
}

void loop()
{
  // reading the output of the soil moisture sensor 
  moistureReading = analogRead(SOIL_PIN);

  // mapping the reading to 0 - 4 
  // represents the amount of LEDs lit up
  moistureMapped = constrain(map(moistureReading, dry, wet, 0, 4), 0, 4);

  // only debugging purposes  
  Serial.print("reading: ");
  Serial.println(moistureReading);
  Serial.print("LEDs on: ");
  Serial.println(moistureMapped);

  // iterating over the LEDs to switch them on or of
  for(int i = 0; i < 4; i++) {

    // if the current LED-count is in the range of LEDs
    // previously mapped from the moisture reading
    // it gets switched on, otherwise it's switched of
    if(i+1 <= moistureMapped) {
      digitalWrite(leds[i], HIGH);      
    } else {
      digitalWrite(leds[i], LOW);
    }
  }

 // updates every second
 delay(1000);   
}
```
And a small demonstration:  
The soil has to be packed quite tightly around the sensor, that's why the fourth LED didn't light up rightaway.  

![Whole setup](../images/week07-image09.gif)  
_(sry for the bad quality, it's just a pretty strongly compressed gif)_

## Downloads, Resources and Tools

### Downloads

[LED Array (.ino)](../assets/week07-LEDArray.ino)  
[LED Array (.zip)](../assets/week07-LEDArray.zip)  
code tests for the failed LED matrix attempts:  
[LED matrix Test (.ino)](../assets/week07-LEDmatrixTest.ino)  
[LED matrix Test (.zip)](../assets/week07-LEDmatrixTest.zip)  


### Resources
  
[Arduino Language Reference](https://www.arduino.cc/reference/en/)  
for the failed LED matrix attempts:  
[LED Backpack library](https://github.com/adafruit/Adafruit_LED_Backpack)  
[GFX Library](https://github.com/adafruit/Adafruit-GFX-Library)  
[Guide on how to use the 1.2'' LED matrix with backpack by adafruit](https://learn.adafruit.com/adafruit-led-backpack/1-2-8x8-arduino-wiring-and-setup)  

### Tools

[Arduino IDE](https://www.arduino.cc/en/software)  