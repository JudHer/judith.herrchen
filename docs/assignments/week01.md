# week 01: Project management

## Summary

Introduction to the course and the first practical assignment: Getting a taste of how to use Markdown Language, how to install and use Git and how to use GitLab to deploy a website for the project documentation.

## Markdown

Markdown can be used to quickly write up texts and add formatting in a simple and easy way. 
We will use it in the process of documenting our course progress.   
  
To get a quick overview, one can use the [Markdown Cheat Sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).

## Git
  
Git is used for version controlling projects. The modifications done to a file can be _committed_ to create an anchor point in the progression of the project, which could be revisited by a _roll back_, if needed.

### Setting up Git  
  
  
**Downloading Git**  
    
Git can be downloaded [here](https://git-scm.com/downloads).

**Setting up User Information**  
    
by running  
`git config --global user.name "[name]"`  
`git config --global user.email "[email address]"`  
 
 **Adding a SSH Key**  
    
As authentication method we will use a SSH key.   
First it needs to be generated, by running  
`ssh-keygen -t rsa -C "<email>"`  
then the generated key can be output by running  
`cat ~/.ssh/id_rsa.pub` (if the public key was placed at the default location)  
  

### Basic commandpalette 

| Command | Description |
|---------------------------|-------------|
|`git clone <repoUrl>` | An existing repository gets cloned into the current working directory.  Now the repository resides locally and new files can be added or exisiting files modified. |
|`git status` | Changes, that were made to the files, will be displayed. |
|`git add <path>` | Stages the files that should be prepared for versioning. (asterisks (\*) for all files)|
|`git rm <path>` | Staged files will be removed to not be included in the commit. |
|`git commit -m "Commit message."` | The changes will be recorded in the version history. |

Interacting with the remote repository

| Command | Description |
|------------|-------------|
|`git fetch` | Gets the version history from the remote. |
|`git pull` | Gets all changes that reside remotely. |
|`git push` | Pushes all changes to the remote. |
  
  

![Git bash example: git status](../images/week01-image01.png)  
_Git bash example: git status_


### Deploying a website with GitLab Pages

The documentation of our projects will be tracked here, with GitLab Pages.  
To set them up the [Template Files](https://gitlab.com/satshas/template-test) can be copied into the local repository and can be modified.  
By pushing those changes Git Lab creates a new pipeline to deploy the website.  
  
And there we go!


## Resources and Materials  

**Resources**  

[Markdown Cheat Sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
[GitLab Markdown Guide](https://about.gitlab.com/handbook/markdown-guide/)

[Git Download](https://git-scm.com/downloads)
[Git Documentation](https://git-scm.com/doc)
[Git Cheat Sheet](https://training.github.com/downloads/github-git-cheat-sheet/)

[GitLab Pages Template](https://gitlab.com/satshas/template-test)

---
**Tools used**

[Markup Editor: Obsidian](https://obsidian.md/)  
[Git](https://git-scm.com)


