# week 06: Input Devices

## Summary

This week we got to know some input devices, how to use them and process the data. I tried out a soil moisture sensor, the DHT11 to meassure the air humidity and temperature and a LDR to meassure the amount of light present. In the end they formed a small "Plant Weather Station"  

## Parts
  
I initially wanted to focus on the soil moisture sensor, but since there were some other sensor that fit the "plant-theme" I also tried them out. So I ended up with a small "Plant Weather Station" to observe the sourrounding parameters a plant might need.  
  
I took several steps to try out every sensor on their own and lastly joined everything together into one.  

### Used Parts:

|Quantity|  |
|-|-|
|1x| Arduino Uno |
|1x| Capacitive Soil Moisture Sensor v1.2 + the connector cables |
|1x| DHT11 |
|1x| LDR |
|1x| Breadboard |
|depending on # of sensors| Jumper Cables |
  
 
## Capacitive Soilmoisture Sensor   

### The Setup

I started out with the soil moisture sensor. The setup is pretty simple: 
The soil moisture sensor came with a connector cable already attached to it, which got connected to the A0 pin, for analog input, and V5 and ground, with some additional jumper cables.

![Setup for testing the Capacitive Soilmoisture Sensor](../images/week06-image01.JPG)
_Setup for testing the Capacitive Soilmoisture Sensor; starring my recently proplifted pothos and a poor water cabbage that got dumped at the riverbanks_  



### The Programming
  
Since the soil moisture sensor would send raw data, I first tried it out by converting none of it. I ran this simple code snippet:

```cpp
#define SOIL_PIN A0

int reading;

void setup() {
  pinMode(SOIL_PIN, INPUT);
  Serial.begin(9600);
}

void loop() {
  reading = analogRead(SOIL_PIN);
  Serial.print("Soil moisture: ");
  Serial.println(reading); // Printing of the raw data the sensor is providing

  delay(100);
}
```

![Serial Monitor printing raw data received from the sensor](../images/week06-image04.png)  
_Serial Monitor printing raw data received from the sensor, left: out of water, right: in water_

... and got values between 816 and 817, when the sensor just lingered in the air and values between 471 and 472, when the sensor were put into water. Though those values weren't that easy to decide on, because they would also change depending on the amount it was lowered in the water or soil.  
  
As a first try I decided to map the values gathered beforehand to a range from 0 to 100, to get percent-like values. Though one had to keep in mind, that the constants I chose, may not work in another scenario, so it should be meassured beforehand. 
  
After adding the _map()_ function, I tested the sensor again, wether there would be a major difference to the first attempt. 

```cpp
#define SOIL_PIN A0

const int dry = 817; // Constant for value when sensor is dry
const int wet = 458; // Constant for value when sensor is wet

void setup() {
  pinMode(SOIL_PIN, INPUT);
  Serial.begin(9600);
}

void loop() {
  int reading = analogRead(SOIL_PIN);

  // mapping the raw data to numbers between 0 and 100 to display them as "percentage" 
  int moistureMapped = map(reading, dry, wet, 0, 100); 
  
  Serial.print("Soil moisture: ");  
  Serial.print(reading); // printing the raw reading 
  Serial.print(" raw, ");
  Serial.print(moistureMapped); // printing the mapped values
  Serial.println(" %");

  delay(100);
}
```

![Serial Monitor printing the moisture sensor data, raw and percentage](../images/week06-image03.png)  
_Serial Monitor printing the soil moisture sensor data; raw and percentage slowly dropping after pulling the sensor out of the water_

It looked fine so I went on testing the next sensor.


## DHT

Next I wanted to combine both the DHT and the soil moisture sensor. Since the DHT11 could also be powered over the 3.3 V pin, there was no need to use the breadboard.  
Reading the DHT is pretty easy, because it comes with a library, which allows one to initialize the reading of the sensor and getting the data back.

```cpp
#include <DHT.h>
#include <DHT_U.h>

#define DHTTYPE DHT11
#define DHT_PIN 3
#define SOIL_PIN A0

const int dry = 817; // Constant for value when sensor is dry
const int wet = 480; // Constant for value when sensor is wet

DHT dht(DHT_PIN, DHTTYPE);

void setup() {
  pinMode(SOIL_PIN, INPUT); 
  pinMode(DHT_PIN, INPUT);
  Serial.begin(9600);
  dht.begin(); // initializing the DHT

  
  Serial.print("| Air Humidity |");
  Serial.print(" Temperature |");
  Serial.println(" Soil Moisture |");
}

void loop() {

  // reading the humidity and temperature with the help of the dht library
  int h = dht.readHumidity();  
  int t = dht.readTemperature();

  int moistureReading = analogRead(SOIL_PIN); 
  // mapping the raw data to numbers between 0 and 100 to display them as "percentage"
  int moistureMapped = map(moistureReading, dry, wet, 0, 100);

  Serial.print("|    ");
  Serial.print(h);
  Serial.print(" %   |   ");
  Serial.print(t);
  Serial.print(" °C  |    "); 
  Serial.print(moistureMapped);
  Serial.println(" %   |");

  delay(2000);
}
```

![Output of the soil moisture sensor and the DHT](../images/week06-image07.gif)  
_The three values air humidity, temperature and soil moisture changing over time_

## Final: Soil moisture sensor, DHT and LDR

Because theme-wise it would fit, I decided to throw the LDR into the mix.  
To power all the sensors I decided to use the breadboard, where I could connect them all. 

### The Setup

![Closeup of the breadboard](../images/week06-image10.JPG)
_Closeup of the breadboard I didn't cut any cables! those small bits are my own!_
![Final setup with soil moisture sensor, DHT and LDR](../images/week06-image09.JPG)
_Final setup with soil moisture sensor, DHT and LDR_   
![Cleaner circuit visualisation](../images/week06-image14.png)
_A cleaner sketch of the whole setup, since the cable colors on the photos are a hot mess_  
  
### The Programming

The code for every sensor had to be combined into one. So I polished up the code a bit.  
During testing the values of the soil moisture sensor sometimes reached 101% or -1%, so I constraint the values with _constraint(map(moistureReading, dry, wet, 0, 100), 0, 100)_ to the values 0 to 100.  

```cpp
#include <DHT.h>
#include <DHT_U.h>

// defining all the pin
#define DHTTYPE DHT11
#define DHT_PIN A2
#define SOIL_PIN A0
#define LDR_PIN A4

// constant values to map the data of the soil moisture sensor to
// they have to be measured again, if the setup is rebuild!
const int dry = 817;
const int wet = 480;
// a counter to repeat the header of the printed table every once in a while
byte turncounter = 0; 

DHT dht(DHT_PIN, DHTTYPE);

// function for printing the table header, nothing exciting 
void printHeader() {
  Serial.print("| Light Level |");
  Serial.print(" Air Humidity |");
  Serial.print(" Temperature |");
  Serial.println(" Soil Moisture |");
}

void setup() {
  // defining the pinmodes, all Input
  pinMode(SOIL_PIN, INPUT);
  pinMode(DHT_PIN, INPUT);
  pinMode(LDR_PIN, INPUT);
  Serial.begin(9600);
  dht.begin();

  // printing the logo for the small Plant Weather Station :)
  Serial.println("         ___");
  Serial.println("-.    __/   \\__");
  Serial.println("\\ `-./  \\,-./  \\ ");
  Serial.println(" \\ -.>--(   )--<  THE PLANT WEATHER STATION ");
  Serial.println("  `-.\\__/`-´\\__/ ");
  Serial.println("        \\___/ \n");
  printHeader();
}

void loop() {
  // reading the data of the DHT11
  int humidityReading = dht.readHumidity();
  int temperatureReading = dht.readTemperature();

  // reading the data of the soil moisture sensor
  int moistureReading = analogRead(SOIL_PIN);

  // mapping the raw data to the pre-measured values for dry and wet 
  // and constraining them to not overstep 0 or 100 %
  int moistureMapped = constrain(map(moistureReading, dry, wet, 0, 100), 0, 100);

  // reading the data of the LDR and mapping them 
  int ldrReading = analogRead(LDR_PIN);
  int ldrMapped = map(ldrReading, 0, 1023, 0, 100);

  // printing the measurements
  Serial.print("|     ");
  Serial.print(ldrMapped);
  Serial.print(" %    |      ");
  Serial.print(humidityReading);
  Serial.print(" %    |     ");
  Serial.print(temperatureReading);
  Serial.print(" °C   |      "); 
  Serial.print(moistureMapped);
  Serial.println(" %   |");

  // printing the header every 15 lines
  if (turncounter == 15 ) {
    turncounter = 0;
    printHeader();
  } else {
    turncounter++;
  }

  delay(2000);
}
```

![Serial Monitor Plant Weather Station](../images/week06-image13.gif)  
_every value changing at some point_

## Downloads, Resources and Tools

### Downloads

[Plant Weather Station download (.ino)](../assets/week06-plantWeatherStation/week06-plantWeatherStation.ino)  
[Plant Weather Station download (.zip)](../assets/week06-plantWeatherStation.zip)  

### Resources
  
[Arduino Language Reference](https://www.arduino.cc/reference/en/)  
[DHT Library](https://github.com/adafruit/DHT-sensor-library)  

### Tools

Arduino Uno  
[Arduino IDE](https://www.arduino.cc/en/software)  