# week 02: 2D and 3D Design

## Summary

First steps in CAD Applications, like Libre CAD and Fusion 360. Libre CAD was used to create a first drawing of a possible design of the container for the electronic parts of the final project idea. Fusion was used to visualize the object, which was planned out beforehand, in a 3D environment.

## 2D Design with Libre CAD 

### Tools and Usage

[Libre CAD](https://librecad.org) can be used to produce 2D designs for all sort of things. It comes with a rich palette of tools:

| tool category | description |
|--------------|-------------|
| ![LibreCAD snapping tools](../images/week02-image15.png) | Snapping-tools: used for snapping the cursor to points in the workspace.  _(left-to-right: freesnapping, grid, endpoints, entity, center, middle, distance, intersection)_ |
| ![LibreCAD restriction tools](../images/week02-image17.png) | Restriction-tools: used for restricting the angle of the line to be either vertical and/or horizontal. |
| ![LibreCAD line tools](../images/week02-image16.png) | Line-tools: used for drawing varius lines in different modi. _(top-to-bottom: line-tools, circle-tools, spline-tools, ellipse-tools, line-editing-tools)_ |
| ![LibreCAD manipulation and measuring tools](../images/week02-image18.png) | Manipulation and measuring tools: used for manipulating (multiple) lines, e.g. by copying or rotating, or measuring distances.  |

### Design process

I first constructed a basic blockout with simple shapes and added construction lines every 30 degree (green) to build up the flowerpetals with ellipses. 

![basic construction of frontpanel](../images/week02-image02.png)
_(basic construction of the front panel)_

Next the inner intersecting part of elipses were trimmed by using the trim tool. To remove the remaining straight lines the divide tool in combination with intersection snapping was used. The line that should be divided has to be selected, then clicking again to place the cutline. After that the partial line can be selected and deleted.

![removing line with divide by intersection](../images/week02-image03.png)
_(line selection before and after the division)_

Next up I added construction lines on a construction layer to transfer the right height for the grooves onto the other panels. The grooves are mostly alternating between 3mm x 6mm and 3mm x 10mm. To fixate the panels in the area of the flowerpetals, I planned small holes in size of the corresponding grooves.

![construction lines](../images/week02-image06.png)
_(Construction lines for transfering the right hight onto the other panels.)_

When one row of grooves was properly aligned, I mirrored the side onto the other. 

![mirroring](../images/week02-image07.png)
_(Mirroring of some grooves.)_

This process was repeated for every panel to form a small box. Futher down the line holes may be added for inserting LEDs or buttons. 

![mirroring](../images/week02-image10.png)
_(Full Layout of the 2D box.)_

[Download the 2D file here >>](../assets/week02-2DDesign01.dxf)

## 3D Design with Fusion 360

### Tools and Usage

[Fusion 360](https://www.autodesk.de/products/fusion-360/overview) can be used to produce both 2D and 3D designs, though here it was only used to create a 3D version of the 2D sketch done beforehand.

| tool category | description |
|--------------|-------------|
| ![Fusion 360 3D orientation](../images/week02-image19.png) | To traverse in the 3D space, the small cube in the top-right corner can be used. Another option is panning with holding down the middle mousebutton and rotating the view by pressing shift + MMB. |
| ![Fusion 360 3D tools](../images/week02-image20.png) | Tools for the creation of basic 3D shapes. _left-to-right_: extruding or cutting shapes from 2D forms; Creating round/rotating forms; Cutting via boolean logic.   |
| ![Fusion 360 3D manipulation](../images/week02-image21.png) | 3D Volume manipulation tools. left-to-right: stretch; round off edge; hollowing; combining objects; cut an object by overlaying another. |
| ![Fusion 360 create sketch](../images/week02-image23.png) | 2D sketch creation. |
| ![Fusion 360 2D sketch tools](../images/week02-image24.png) | 2D sketching tools. similar to Libre CAD.  |
| ![Fusion 360 2D sketch manipulation](../images/week02-image25.png) | Manipulation of 2D shapes. Rounding off corners; Cutting/Deleting lines; Copy/Shrink curves.   |
| ![Fusion 360 2D restriction tools](../images/week02-image27.png) | Restrictions that can be applied to the lines of a 2D sketch, e.g. it can force lines to always be orthogonal to each other. (Only a selection of tools)  |
| ![Fusion 360 measuring tool](../images/week02-image28.png) | Tool for e.g. measuring distances.  |

### Design process
  
I imported the .dxf file and cleaned up the sketch. I duplicated the side plane and applied 3 mm volume to every part, by extruding them. 

![from 2D sketch to 3D](../images/week02-image12.png)

After that I assembled the different parts to form a box.  
Every single piece are made into a componenten and gets assembled by defining a joint on the adjacent piece (shortcut _J_).  

![the finished 3D Model of the box](../images/week02-image13.png)

## Downloads  
  
[Download the 2D file here (.dxf) >>](../assets/week02-2DDesign01.dxf)  
[Download the 3D file here (.f3d) >>](../assets/week02-3DDesign.f3d)  
