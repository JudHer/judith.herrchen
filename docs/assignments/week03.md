# week 03: Laser Cutting

## Summary

Laser cutting time! Steps taken to make a parametric design in Fusion 360 and preparing it for cutting it with a laser cutter.  

## Parametric Designs

### Theory

A parametric design is driven by the use of custom parameters to represent a certain feature. Those parameters can be assigned to parts of the design. So changing the value of those parameters also means a change in the design itself.  
  
To make use of parameters in Fusion 360, they can either be created beforehand or can be set e.g. by typing the exact value during drawing a line.  
The menu for adding or modifing parameters can be found under _Modify > Change Parameters_.  
In this Context one has access to the _User Parameters_ (manually set parameteres) and the _Model Parameters_ (The parameters automatically defined while making the sketch).   
After marking a parameter as favourite, it will be available in autocompletion while typing.  

### The Design

First up I scribbled some teeny tiny concept sketches, to have a clearer idea of which parts should be sketched out in Fusion.  
My ideas was geard towards a small and simple seed storing container, with compartments for every season those seeds could be planted in.  

![concept sketch](../images/week03-image18.JPG)

After jumping into Fusion I defined parameters that would make up the long and short side of the bottom plane. The short side was simply defined by a parameter "shortSide". 
The longer side would be dependend on the inner container count, the container width and the material thinkness, due to the partition boards between the inner compartments.  

```LongSide = containerWidth * containerCount + materialThickness * ( containerCount - 1 )```  
  
![menu for adding parameters](../images/week03-image01.png)  
_Menu for adding custom user parameters_
  
One thing that should also be changeable by the applied parameters, is the so called kerf the laserbeam will leave behind, while burning the material.  
   
An important part was to test the parameters every now and then during the process, to make sure everything is applied in the correct way and adjustable without much hassle.  
  
The parameters were defined, so I blocked out a rectangle for the bottomplane and added joints on the corners of the bottomplane. Next I created a rectangular pattern along the sides of the bottom plane to add the rest of the joints.  
The rectangular pattern tool can be found in the sketch-section under _Create > Rectangular Pattern_.

![rectangular pattern creation](../images/week03-image04.png)  
_Creating a rectangular pattern_

The "gooveCount" is defined by ```( longSide / groovesWidth / 2 ) - 2``` ( -2 because the two joints on the corners were already set)  
and the distance between the grooves is defined by ```groovesWidth * 2```    
  
To speed things up the joints were mirrored onto the other sides.  
The side planes were added with a similar workflow, only the groovecount of the opposite side had to be decreased by one, so the joints could fit into the others.  
The toppart of the longer sideplanes got some deeper grooves to fit the partition boards inside.  
  
![Layout after every piece was outlined](../images/week03-image10.png)  
_Layout after every piece was outlined_  
  
To test wether all joints were aligned properly, I apllied volume to the boards and joined them together.  

![Assembled pieces](../images/week03-image11.png)  
_The assembled box in Fusion_  
  
Now I just needed to do the part that should be engraved or done as a marking.    
So I jumped into inkscape and drew out some simple icons to represent the different seasons.  
  
![Icons for the different seasons](../images/week03-image22.png)  
_winter, frosty spring, spring, early summer, summer and autumn_  
  
Since I had some issues importing the svg images into fusion I converted the svg files with an online tool ([convertio.co](https://convertio.co/de/)) into dfx files.
While arranging the parts in Fusion to make them ready to be cut, I also ran into problems moving the icons around the workspace. So I just added placeholders for the icons and took the last steps in LibreCAD.  

![Arranged for cutting in Fusion and LibreCAD](../images/week03-image13.PNG)  
_right: Arrangement in Fusion, left: Arrangement in LibreCAD, now with the icons_  

So finally - the cutting!


## Usage of a laser cutter

### Theory
  
A laser cutter is used for very precise cutting of materials such as wood or acrylic.  
The laserbeam is able to either burn completly through the material or to just "touch" the surface.  
The result can be controlled by the following factors:   
  
**Speed** - if the laserbeam is moved very fast over the surface, the specific point just gets burned a slight bit  
**Power** - the higher the power, the stronger the burning and the deeper the cut  
**Frequency** - the frequency needs to be changed according to the material, wood is cut on a lower frequency to not burn it, whereas acrylic needs a higher frequency  
  
  
### The Cutting
  
Before being able to cut, I had to adjust the kerf, so that it would be around 0.2 mm in total.  
  
![Kerf adjustments](../images/week03-image15.PNG)  
_Joints after the kerf adjustment_  
  
To test wether the joints would fit, they were testet on a small sample piece with just one joint.  
  
After the testing was done, the file with the actual design could be prepared.  
In order to tell the laser cutter which parts of the design should be cut and which parts should be engraved or marked, the layout had to be colour mapped accordingly.  
To define the colours of each part, layers with a colour property were created and assigned to the pieces that should be either cut, engraved or marked.  
Since I accidentally imported the dxf files for the icons as blocks in LibreCAD, the colours couldn't be assigned via the layer-property and had to be defined by specifying a colour directly in the _Print Colour_ section. Lastly the small connecting bits inbetween the joints had to be removed (selecting all lines, typing _trim_ as command and clicking the lines that had to be trimmed) and all the remaining lines had to be joined together, so the laserbeam would be moved in a continuous line.  
    
![Colourmapping of the layout](../images/week03-image16.PNG)  
_Colourmapping to prepare for the different modes_  
  
Hitting _Ctrl + P_ leads to the printing menu, where the properties could be defined. In this case the height and width of the cutting-surface the parts should fit into and the colour mapping.  

![Colour mapping in the prperties window](../images/week03-image17.PNG)  
_Colour mapping over in the properties window of the laser cutter_  
  
The cutting job got send to the laser cutter (with the handy feature, that the estimated time the job will take is displayed):    
![Cutting job and estimated time to cut](../images/week03-image23.jpg)  
_Cutting job and estimated time to cut_  
  
With all that done - let the cutting begin!  
  
![Marking](../images/week03-image24.jpg)  
_Marking of an icon_  

![Finished cutting](../images/week03-image25.jpg)  
_Finished!_  
  
And to the assembling:  

Most pieces slotted into place really nicely. Only one piece didn't really want to take its place.  

![The stubborn piece](../images/week03-image19.JPG)  
_The stubborn piece_  
  
So I decided to trim the inner parts of the joints just a tad bit.  
And now everything fit together snuggly!  
  
![Assembled box!](../images/week03-image212.png)  
_The assembled box! empty and in action_  

And for comparison the marking and the engraving part:  
  
![engraving and marking next to each other](../images/week03-image220.JPG)  
_right: marking, left: engraving_  
  

## Downloads, Ressources and Tools

### Downloads
  
[Laser Cut Design (.f3d)](../assets/week03-LaserCutDesign.f3d) (not properly layed out to be able to cut it, because it mixed up the parameters)  
[Laser Cut Design (.dxf)](../assets/week03-LaserCutDesign.dxf)  

### Tools used: 
  
[Libre CAD](https://librecad.org)  
[Fusion 360](https://www.autodesk.de/products/fusion-360/overview)  
[Ink Scape](https://inkscape.org)  
[convertio.co (Online tool)](https://convertio.co/de/)  
