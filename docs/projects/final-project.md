# Final Project

## Preperations

I first sketched out some design ideas, to get a better understanding on how everything should look in the end and where to place the parts that were needed. While doing it I quickly realised that stuffing a larger breadboard and an Arduino Uno into a fitted box results in a pretty bulky looking thing to hang onto a plantpot.  
So the decision was made to use a smaller breadboard and an Arduino Nano.  

![First idea sketches](../images/week09-image01.png)  
_First idea sketches_  

## Components

|Quantity|  |
|-|-|
|1x| Arduino Nano |
|1x| Arduino Nano I/O Shield (don't know whether this is the right name for it?)|
|1x| Capacitive Soil Moisture Sensor v1.2 + the connector cables |
|1x| Temperature Sensor DS18B20 |
|1x| LDR |
|1x| small Breadboard |
|some| Jumper Cables |
|1x| 4.7kOhm Resistor |
|1x| 10kOhm Resistor |

## The Design

### The casing (Laser cutting)

The case was first sketched out in Fusion 360, with a similar workflow like in [week 03](https://judher.gitlab.io/judith.herrchen/assignments/week03/). The size had to be adjusted during the process, so keeping the sketch parametric really helped.  
The small cutout in the front should be a frame for the LED-Matrix I would use for the visualisation of the measured parameters temperature, soil moisture and light level. 
![Case layout in Fusion](../images/week09-image02.png)  
_Case layout in Fusion_  

![Front Icons](../images/week09-image16.png)   
_Front icons to assign meaning to each bar_  

To make the box a bit more visually appealing I decided to try out a rounded top with the help of kerf bending. The first time I tried bending the wood it made little cracking sounds so I tried softening it with some steam. And it worked out surprisingly well!  

![Bended toppart](../images/week09-image05.JPG)  
![Bended toppart](../images/week09-image05-1.JPG)  
_The bended top part of the box_  

The box had to be held together by some elastics for some time, so it would keep it's shape after bending the softened wood.  

After that I tried screwing on the LED-Matrix. It fitted okay-ish, but the countersunk screws wouldn't quite fit. Maybe some nicer looking screws could be used, but it held together.

![Screwed on LED-Matrix](../images/week09-image07.JPG)  
_Screwed on LED-Matrix_  

### The hook (3D printing)

To be able to hang the box onto a plantpot I planned to add a small hook on the backside. I measured the wall width of some of my plantpots, but the range was quite wide so I first thought about making a kind of bendable clip. But in the end I discarded that decision and went with a stiff but broader hook, since I didn't know how bendable the endresult would have been. Since it wasn't quite a shape that could be made with lasercutted parts I decided on 3D printing it.  

As well as for the lasercutting design I used Fusion 360 for moddeling the hook.  
  
![Leaf Hook](../images/week09-image03.png)  
_Small backside hook with a hint to a shape of a leaf_  

The hook fitted perfectly on the edge of one of my thicker pots and therefore  on most of the thin ones. 

![Hook on a plantpot](../images/week09-image06.JPG)  
_The bare hook on one of my plantpots_  

Since it should give a hint to a leave shape I painted it a greenshade.

![Painted hook](../images/week09-image10.JPG)  
_The painted leaf-hook_  


## The Electronics Setup

Since I already used the soil moisture sensor and the LDR in the [Input device week](https://judher.gitlab.io/judith.herrchen/assignments/week06/) I already knew how to set them up. The new thing was the temperature sensor, but the [Datasheet](https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf) helped a lot. Even though the LED-Matrix tries on week 07 weren't successful the [Adafruit guide](https://learn.adafruit.com/adafruit-led-backpack/1-2-8x8-arduino-wiring-and-setup) came in handy again 

![Setup Try](../images/week09-image08.JPG)  
_First tests_  

The connections for each sensor / the LED-Matrix:  

Aurduino - LDR  
```
A2 ──────┬───────────────────┐
       10kOhm             LDR
GND ─────┘                  │
                            │
5V ─────────────────────────┘
```

Aurduino - DS18B20  
```
5V ──────┬───────────────────┐
       4.7kOhm               │
D3 ──────┴─────────────────DS18B20     
                             │
GND ─────────────────────────┘
```

Aurduino - Soil Moisture Sensor  
```
A0 ──────────────────────────┐
                             │
5V ─────────────── Soil Moisture Sensor      
                             │
GND ─────────────────────────┘
```

Aurduino - LED-Matrix  
```
A5 / SCL ─────────────────────────┐
A4 / SDA ──────────────────────  LED-
GND ─────────────────────────── Matrix 
5V ───────────────────────────────┘
```

After the code was transfered I also tried running it with a battery and everything worked out fine. 

![Setup Try](../images/week09-image09.JPG)  
_Setup test with battery_  


## The Programming

The code is rather simple since the temperature sensor and the LED-Matrix came with their own libraries ([DallasTemperature](https://www.arduinolibraries.info/libraries/dallas-temperature), [Adafruit_GFX](https://github.com/adafruit/Adafruit-GFX-Library), [Adafruit_LEDBackpack](https://github.com/adafruit/Adafruit_LED_Backpack)) and the raw input of the soil moisture sensor and the LDR only had to be mapped to the amount of LEDs that should be lit to display the according level. I oriented myself on the code I've already written for [week 06](../assignments/week06) and [week 07](../assignments/week07). The examples on how to use the LED-Matrix provided by Adafruit also came in quite handy to get a picture what could be done with it. (They can be opened via the Arduino IDE under _File > Examples > Adafruit LED Backpack Library_)
  
Here the final code I used:  
(Comments are inline)


```cpp
#include <OneWire.h>
#include <DallasTemperature.h>

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_LEDBackpack.h>

Adafruit_8x8matrix matrix = Adafruit_8x8matrix();

// defining all the pins
#define SOIL_PIN A0
#define LDR_PIN A2
#define TEMP_PIN 3

OneWire oneWire(TEMP_PIN); 
DallasTemperature sensors(&oneWire);

// constant values to map the data of the soil moisture sensor to
// they have to be measured again, if the setup is rebuild!
const int dry = 817;
const int wet = 480;
// just two variables to make the configuration easier 
// 0 and 30 degree are picked quite random 
// I hope the temperature of my room will never drop to 0 degrees 
const int maxTemp = 30;
const int minTemp = 0;

void setup() {
  // defining the pinmodes, all Input
  pinMode(SOIL_PIN, INPUT);
  pinMode(LDR_PIN, INPUT);
  sensors.begin();
  matrix.begin(0x70);  
  // The LED-Matrix has to be rotated since I screwed it on flipped
  matrix.setRotation(1);
}

void loop() {

  // reading the data of the LDR 
  int ldrReading = analogRead(LDR_PIN);
  
  // if it's bright enough measure the other stuff and display it
  if(ldrReading > 102) {       
    
    // reading the data of the soil moisture sensor
    int moistureReading = analogRead(SOIL_PIN);
    // request reading of the temperature sensor
    sensors.requestTemperatures();
    float temp = sensors.getTempCByIndex(0);
    
    // mapping the ldr reading that was measured before hand   
    // it gets mapped from 0 to 8 so it can be displayed on the 8 LEDs of the LED-Matrix
    int lightMatrixMap = map(ldrReading,0, 1023, 0, 8); 
    
    // mapping the soil moisture reading to the pre-measured values for dry and wet 
    // same as with the LDR mapping, from 0 to 8 because of the LED count
    // additionally the values had to be constrain so to not overstep 0 or 8  
    // since dry and wet doesn't reach from 0 to 1023 and can overstep when measured
    int soilMatrixMap = constrain(map(moistureReading, dry, wet, 0, 8), 0, 8);

    // since apparently there has to be a check for the connection 
    // the variable gets initialised with 0 and later assign the actual temperature value
    // like before it gets mapped to 0 and 8 and gets constrained
    int tempMatrixMap = 0;
    if(temp != DEVICE_DISCONNECTED_C) 
    {
      tempMatrixMap = constrain(map(temp, minTemp, maxTemp, 0, 8), 0, 8);
    }
     
    // The matrix gets cleared before drawing so the old bars disappear
    // so all LEDs get switched off
    matrix.clear();
    // Temperature Bar
    matrix.drawRect(0,0, tempMatrixMap, 2, LED_ON);
    // Soil Moisture Bar
    matrix.drawRect(0,3, soilMatrixMap, 2, LED_ON);
    // Light Level Bar
    matrix.drawRect(0,6, lightMatrixMap, 2, LED_ON);
    // when all changes are made the according LEDs get switched on
    matrix.writeDisplay(); 
  } else {
    // Matrix doesn't displays anything if it's too dark 
    // don't want to get disturbed in my sleep
    matrix.clear();
    matrix.writeDisplay();
  }
  delay(2000);
}
```

![Debuging console logs](../images/week09-image04.png)  
_Some logging for debuging_  

## Putting everything together

After flashing the code, the whole thing could be put together.  
The Arduino and the small breadboard only get held on the backside by tape. This is one thing I would've liked to improve, so they maybe could've been held by some kind of clip.   
     
 ![arrangement in the box](../images/week09-image11.JPG)  
_First arrangement in the box_   

![cabels on the outside](../images/week09-image12.JPG)  
_The cables for the sensors were routed through the holes on the backside_   

The Last touch was to add the small wooden flower, that contains the sensors:  
  
![flower!](../images/week09-image14.JPG)  
_✿ ✿ ✿_   
  
So the last thing to do was to test it in a harsh environment - my window sill  
  
![weather station in action](../images/week09-image15.JPG)  
_The plant weather station in action_   
![the sensors](../images/week09-image17.JPG)  
_The sensors_   
  

## Additions and things that could be made better

All in all everything worked out, but only with a bit of fiddeling. Screws weren't sitting the rightway, the sum of the cables were more stiff than expected, the routing of the cables on the backside isn't really elegant - there are many imperfections.  

Things that I would change or add are 1. small holes on the backside of the case to screw the hook on more easily, 2. clasps on the inside to hold the Arduino and the breadboard, 3. small channels left and right on the hook to route the cables through, 4. a visually more appealing way to stick the flower onto the sensors.

Apparently I also misplaced the small hinge I planned on using for the batterypack lid. If I'll find it again I'll add it.  
  
Anyways,  
in spite of those trying times and the roadblocks, this course was really fun and inspiring and was definitely one of the things that got me through the semester and prevented me from going insane :D  
All the newly learned stuff opened up some kind of new perspective and I hope someday I could expand on it.  
So a huge thank you!  

## Downloads, Resources and Tools

### Downloads
Box:  
[finalProject Plant Weather Station box (.dxf) ](../assets/finalProject-plantWeatherStation-box.dxf)  
[finalProject Plant Weather Station box (.f3d) ](../assets/finalProject-plantWeatherStation-box.f3d)  
Leafhook:  
[finalProject Plant Weather Station leafhook (.stl) ](../assets/finalProject-plantWeatherStation-leafHook.stl)   
[finalProject Plant Weather Station leafhook (.gcode) ](../assets/finalProject-plantWeatherStation-leafHook.gcode)  
Code:  
[finalProject Plant Weather Station (.ino) ](../assets/finalproject-plantWeatherStation/finalproject-plantWeatherStation.ino)  
[finalProject Plant Weather Station (.zip) ](../assets/finalproject-plantWeatherStation.zip)   

### Resources
  
[Arduino Language Reference](https://www.arduino.cc/reference/en/)  
LED-Matrix:   
[LED Backpack library](https://github.com/adafruit/Adafruit_LED_Backpack)  
[GFX Library](https://github.com/adafruit/Adafruit-GFX-Library)  
[Guide on how to use the 1.2'' LED matrix with backpack by adafruit](https://learn.adafruit.com/adafruit-led-backpack/1-2-8x8-arduino-wiring-and-setup)  
Temperatur sensor:  
[DallasTemperature Library](https://www.arduinolibraries.info/libraries/dallas-temperature)  
[DS18B20 Datasheet](https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf)  

### Tools
  
[Libre CAD](https://librecad.org)  
[Fusion 360](https://www.autodesk.de/products/fusion-360/overview)  
[Ink Scape](https://inkscape.org)  
[convertio.co (Online tool)](https://convertio.co/de/) (for converting svg to dxf)  
[Arduino IDE](https://www.arduino.cc/en/software)  