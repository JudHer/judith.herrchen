# Home

## week 01: [Project Management](./assignments/week01.md)  
**Small Summary**  
Introduction to the course and the first practical assignment: Getting a taste of how to use Markdown Language, how to install and use Git and how to use GitLab to deploy a website for the project documentation.  
  
## week 02: [2D and 3D Design](./assignments/week02.md)  
**Small Summary**  
First steps in CAD Applications, like Libre CAD and Fusion 360. Libre CAD was used to create a first drawing of a possible design of the container for the electronic parts of the final project idea. Fusion was used to visualize the object, which was planned out beforehand, in a 3D environment.

## week 03: [Laser Cutting](./assignments/week03.md) 
**Small Summary**  
Laser cutting time! Steps taken to make a parametric design in Fusion 360 and preparing it for cutting it with a laser cutter. 
  
## week 04: [3D Printing](./assignments/week04.md)
**Small Summary**  
This week we learned the fundamentals of 3D printing. By getting an introduction to the configuration options of a 3D printer and in the end creating our own model, again in Fusion 360, preparing it with Cura to be ready to be printed.  
  
## week 05: [Embedded programming](./assignments/week05.md)
**Small Summary**  
This week we got to know the Arduino Uno and wrote our first little program, which included serial communication, reading a button as input device and writing a LED as output device.  

## week 06: [Input Devices](./assignments/week06.md)  
**Small Summary**  
This week we got to know some input devices, how to use them and process the data. I tried out a soil moisture sensor, the DHT11 to meassure the air humidity and temperature and a LDR to meassure the amount of light present. In the end they formed a small "Plant Weather Station"   

## week 07: [Output Devices](./assignments/week07.md)  
**Small Summary**   
After the input device week there's of course the output device week! This week we got to know some output devices and how to use them. In this example I initially tried using a LED matrix, which failed due to problem I couldn't solve. Soo there had to be some improvisations.  
  
## week 08: [Electronics Design](./assignments/week08.md)  
**Small Summary**  
This week we learned the very basics of electronics design. For planning out the board we used [Eagle](https://www.autodesk.de/products/eagle/overview). 

## week 09 - 11: [The Final Project](./projects/final-project.md) 